import pygame
import sys 
from PIL import Image
import time
import config

config.play_music()

# main loop of the game
while not config.game_over_1 or not config.game_over_2:
	for event in pygame.event.get():
		# enabling close button
		if event.type == pygame.QUIT:                               
			pygame.quit()
			exit(0)
		# detecting movement for config.player_1
		if not config.done:
		# when movement keys are pressed
			if event.type == pygame.KEYDOWN:                            
				if event.key == pygame.K_LEFT:
					config.pressed_left_1 = True
				elif event.key == pygame.K_RIGHT:
					config.pressed_right_1 = True
				elif event.key == pygame.K_DOWN:
					config.pressed_down_1 = True
				elif event.key == pygame.K_UP:
					config.pressed_up_1 = True
		# when movement keys are released
			if event.type == pygame.KEYUP:                              
				if event.key == pygame.K_LEFT:
					config.pressed_left_1 = False
				elif event.key == pygame.K_RIGHT:
					config.pressed_right_1 = False
				elif event.key == pygame.K_DOWN:
					config.pressed_down_1 = False
				elif event.key == pygame.K_UP:
					config.pressed_up_1 = False
		# detecting movement for config.player_2
		else:
			#when movement keys are pressed
			if event.type == pygame.KEYDOWN:                            
				if event.key == pygame.K_a:
					config.pressed_left_2 = True
				elif event.key == pygame.K_d:
					config.pressed_right_2 = True
				elif event.key == pygame.K_s:
					config.pressed_down_2 = True
				elif event.key == pygame.K_w:
					config.pressed_up_2 = True
		# when movement keys are released
			if event.type == pygame.KEYUP:                              
				if event.key == pygame.K_a:
					config.pressed_left_2 = False
				elif event.key == pygame.K_d:
					config.pressed_right_2 = False
				elif event.key == pygame.K_s:
					config.pressed_down_2 = False
				elif event.key == pygame.K_w:
					config.pressed_up_2 = False
	
	config.screen.fill(config.floor_color)
	
	# draw partitions
	config.make_partitions()	
	
	# display traps
	config.display_traps()
	
	# display moving obstacles
	config.display_ships()
	
	# display end
	config.screen.blit(config.end1.Image, (config.end1.pos[0],config.end1.pos[1]))

	# PLAYER 1
	if not config.done:
		# execute obstacle motion for config.player_1
		config.obstacle_motion(config.incr_1)
		
		# move player 1
		if config.pressed_left_1 and config.player_1.pos[0] > 0:                       
			config.player_1.pos[0] = config.player_1.pos[0] - config.player_1.speed
		if config.pressed_right_1 and config.player_1.pos[0] < config.WIDTH - config.player_1.Image.get_width():
			config.player_1.pos[0] = config.player_1.pos[0] + config.player_1.speed
		if config.pressed_down_1 and config.player_1.pos[1] < config.HEIGHT - config.player_1.Image.get_height():
			config.player_1.pos[1] = config.player_1.pos[1] + config.player_1.speed
		if config.pressed_up_1 and config.player_1.pos[1] > 0:
			config.player_1.pos[1] = config.player_1.pos[1] - config.player_1.speed
		config.screen.blit(config.player_1.Image, (config.player_1.pos[0], config.player_1.pos[1]))
	# PLAYER 2
	else:
		# execute obstacle motion for config.player_2
		config.obstacle_motion(config.incr_2)
		
		# move player 2	
		if config.pressed_left_2 and config.player_2.pos[0] > 0:                       
			config.player_2.pos[0] = config.player_2.pos[0] - config.player_2.speed
		if config.pressed_right_2 and config.player_2.pos[0] < config.WIDTH - config.player_2.Image.get_width():
			config.player_2.pos[0] = config.player_2.pos[0] + config.player_2.speed
		if config.pressed_down_2 and config.player_2.pos[1] < config.HEIGHT - config.player_2.Image.get_height():
			config.player_2.pos[1] = config.player_2.pos[1] + config.player_2.speed
		if config.pressed_up_2 and config.player_2.pos[1] > 0:
			config.player_2.pos[1] = config.player_2.pos[1] - config.player_2.speed
		config.screen.blit(config.player_2.Image, (config.player_2.pos[0], config.player_2.pos[1]))
	
	if not config.done:
		# check collisions of config.player_1
		a1 = config.check_collision_1(config.player_1, config.ship_1)
		a2 = config.check_collision_1(config.player_1, config.ship_2)                    
		a3 = config.check_collision_1(config.player_1, config.ship_3)                    
		a4 = config.check_collision_1(config.player_1, config.ship_4)                    
		a5 = config.check_collision_1(config.player_1, config.ship_5)                    
		a6 = config.check_collision_1(config.player_1, config.ship_6)                    
		a7 = config.check_collision_2(config.player_1, config.trap_1)                    
		a8 = config.check_collision_2(config.player_1, config.trap_2)                    
		a9 = config.check_collision_2(config.player_1, config.trap_3)                    
		a10 = config.check_collision_2(config.player_1, config.trap_4)                    
		a11 = config.check_collision_2(config.player_1, config.trap_5) 
		if a1 or a2 or a3 or a4 or a5 or a6 or a7 or a8 or a9 or a10 or a11:
			config.done = True
			config.neutralise()
			config.reset_1()
			config.end1.change_end_1()
			config.game_over_1 = True
			config.reset_2()
			config.initialise_checkpoints()
			config.screen.blit(config.player_2.Image, (config.player_2.pos[0], config.player_2.pos[1]))
	
	else:	
		# check collisions of config.player_2
		b1 = config.check_collision_1(config.player_2, config.ship_1)
		b2 = config.check_collision_1(config.player_2, config.ship_2)                    
		b3 = config.check_collision_1(config.player_2, config.ship_3)                    
		b4 = config.check_collision_1(config.player_2, config.ship_4)                    
		b5 = config.check_collision_1(config.player_2, config.ship_5)                    
		b6 = config.check_collision_1(config.player_2, config.ship_6)                    
		b7 = config.check_collision_2(config.player_2, config.trap_1)                    
		b8 = config.check_collision_2(config.player_2, config.trap_2)                    
		b9 = config.check_collision_2(config.player_2, config.trap_3)                    
		b10 = config.check_collision_2(config.player_2, config.trap_4)                    
		b11 = config.check_collision_2(config.player_2, config.trap_5) 
		if b1 or b2 or b3 or b4 or b5 or b6 or b7 or b8 or b9 or b10 or b11:
			config.done = False
			config.neutralise()
			config.reset_2()
			config.end1.change_end_2()
			config.game_over_2 = True
			config.reset_1()
			config.initialise_checkpoints()
			config.screen.blit(config.player_1.Image, (config.player_1.pos[0], config.player_1.pos[1]))
	
	if not config.done:
		# player 1 finishes a round
		if config.check_collision_3(config.player_1, config.end1):
			config.done = True
			config.incr_1 += 1
			config.neutralise()
			config.reset_1()
			config.end1.change_end_1()
			if config.game_over_2:
				config.game_over_2 = False
			if config.game_over_1:
				config.game_over_1 = False
			config.reset_2()
			config.initialise_checkpoints()
			config.screen.blit(config.player_2.Image, (config.player_2.pos[0], config.player_2.pos[1]))
	
	else:
		# player 2 finishes a round
		if config.check_collision_3(config.player_2, config.end1):
			config.done = False
			config.incr_2 += 1
			config.neutralise()
			config.reset_2()
			config.end1.change_end_2()
			if config.game_over_1:
				config.game_over_1 = False
			if config.game_over_2:
				config.game_over_2 = False
			config.reset_1()
			config.initialise_checkpoints()
			config.screen.blit(config.player_1.Image, (config.player_1.pos[0], config.player_1.pos[1]))
	
	if not config.done:	
		# update player 1's score
		if config.player_1.pos[1] == 775 and config.checkpoint[0]:
			config.no.score1 += 50
			config.checkpoint[0] = False
		if config.player_1.pos[1] == 650 and config.checkpoint[1]:
			config.no.score1 += 50 
			config.checkpoint[1] = False 
		if config.player_1.pos[1] == 575 and config.checkpoint[2]:
			config.no.score1 += 25
			config.checkpoint[2] = False
		if config.player_1.pos[1] == 475 and config.checkpoint[3]:
			config.no.score1 += 25
			config.checkpoint[3] = False
		if config.player_1.pos[1] == 375 and config.checkpoint[4]:
			config.no.score1 += 50 
			config.checkpoint[4] = False
		if config.player_1.pos[1] == 325 and config.checkpoint[5]:
			config.no.score1 += 2
			config.checkpoint[5] = False
		if config.player_1.pos[1] == 250 and config.checkpoint[6]:
			config.no.score1 += 75
			config.checkpoint[6] = False
		if config.player_1.pos[1] == 200 and config.checkpoint[7]:
			config.no.score1 += 25
			config.checkpoint[7] = False
		if config.player_1.pos[1] == 125 and config.checkpoint[8]:
			config.no.score1 += 50  
			config.checkpoint[8] = False
		if config.player_1.pos[1] == 50 and config.checkpoint[9]:
			config.no.score1 += 50	
			config.checkpoint[9] = False
		
	else:
		# update Player 2's score
		if config.player_2.pos[1] == 125 and config.checkpoint[0]:
			config.no.score2 += 50   
			config.checkpoint[0] = False
		if config.player_2.pos[1] == 200 and config.checkpoint[1]:
			config.no.score2 += 50
			config.checkpoint[1] = False
		if config.player_2.pos[1] == 250 and config.checkpoint[2]:
			config.no.score2 += 25
			config.checkpoint[2] = False
		if config.player_2.pos[1] == 325 and config.checkpoint[3]:
			config.no.score2 += 75
			config.checkpoint[3] = False
		if config.player_2.pos[1] == 375 and config.checkpoint[4]:
			config.no.score2 += 25
			config.checkpoint[4] = False
		if config.player_2.pos[1] == 450 and config.checkpoint[5]:
			config.no.score2 += 50
			config.checkpoint[5] = False
		if config.player_2.pos[1] == 525 and config.checkpoint[6]:
			config.no.score2 += 25
			config.checkpoint[6] = False
		if config.player_2.pos[1] == 650 and config.checkpoint[7]:
			config.no.score2 += 25
			config.checkpoint[7] = False
		if config.player_2.pos[1] == 725 and config.checkpoint[8]:
			config.no.score2 += 50
			config.checkpoint[8] = False
		if config.player_2.pos[1] == 800 and config.checkpoint[9]:
			config.no.score2 += 50
			config.checkpoint[9] = False
	
	# display_score()
	config.display_score()
	
	pygame.display.update()
	 
# end of main game loop

# display GAME OVER
config.g_o()
pygame.display.update()
pygame.time.wait(5000)
