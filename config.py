import pygame
import sys 
from PIL import Image
import time

pygame.mixer.pre_init(44100,16,2,4096)
pygame.init()

# declaration of variables
WIDTH = 1500
HEIGHT = 900

BLUE = (0,0,255)
RED = (255,0,0)
GREEN = (0,255,0)
floor_color = (255,204,102)

width_part = 1500
height_part = 50

# declaration of classes
class Player_1:
	def __init__(self):
		self.size = 50
		self.pos = [WIDTH//2, HEIGHT - 50]
		self.speed = 0.5
		self.Image = pygame.image.load(r'images/nibbles.png')

class Player_2:
	def __init__(self):
		self.size = 50
		self.pos = [WIDTH//2, 0]
		self.speed = 0.5
		self.Image = pygame.image.load(r'images/jerry.png')

class enemy_1:
	def __init__(self, pos, speed):
		self.size = 50
		self.pos = pos
		self.speed = speed
		self.Image = pygame.image.load(r'images/tom.png')

class enemy_2:
	def __init__(self, pos, speed):
		self.size = 50
		self.pos = pos
		self.speed = speed
		self.Image = pygame.image.load(r'images/tom2.png')

class enemy_3:
	def __init__(self, pos):
		self.size = 50
		self.pos = pos
		self.Image = pygame.image.load(r'images/trap.png')
class score:
	def __init__(self):
		self.score1 = 0
		self.score2 = 0
class end:
	def __init__(self):
		self.Image = pygame.image.load(r'images/end.png')
		self.pos = [WIDTH - self.Image.get_width(), 0]
	def change_end_1(self):
		self.pos[0] = 0
		self.pos[1] = HEIGHT - self.Image.get_height()
	def change_end_2(self):
		self.pos[0] = WIDTH - self.Image.get_width()
		self.pos[1] = 0

# declaration of variables
screen = pygame.display.set_mode((WIDTH, HEIGHT))

game_over_1 = False                  
game_over_2 = False

done = False

incr_1 = 0
incr_2 = 0

time_1 = None
time_2 = None

pressed_up_1 = False
pressed_down_1 = False
pressed_right_1 = False
pressed_left_1 = False
pressed_up_2 = False
pressed_down_2 = False
pressed_right_2 = False
pressed_left_2 = False

player_1 = Player_1()
player_2 = Player_2()

no = score()
end1 = end()

clock = pygame.time.Clock()

checkpoint = [True, True, True, True, True, True, True, True, True, True, True]

# declaring moving obstacles
ship_1 = enemy_1([50,50],0.5)               
ship_2 = enemy_2([1450,125],0.2)
ship_3 = enemy_1([50,250],0.3)
ship_4 = enemy_2([1450,375],0.6)
ship_5 = enemy_1([50,650],0.25)
ship_6 = enemy_1([50,775],0.45)

# declaring stationary obstacles
trap_1 = enemy_3([900,200])                
trap_2 = enemy_3([1200,260])
trap_3 = enemy_3([200,325])
trap_4 = enemy_3([800,450])
trap_5 = enemy_3([400,580])
    

def make_partitions():
	pygame.draw.rect(screen, BLUE, (0, HEIGHT - height_part,
	                  width_part, height_part))
	pygame.draw.rect(screen, BLUE, (0, 725,
	                  width_part, height_part))
	pygame.draw.rect(screen, BLUE, (0, 525,
	                  width_part, height_part))
	pygame.draw.rect(screen, BLUE, (0, 325,
	                  width_part, height_part))
	pygame.draw.rect(screen, BLUE, (0, 200,
	                  width_part, height_part))
	pygame.draw.rect(screen, BLUE, (0, 0,
	                  width_part, height_part))


def obstacle_motion(incr):    
		if ship_1.pos[0] >= ship_1.size and ship_1.pos[0] <= WIDTH - ship_1.size:
			ship_1.pos[0] += ship_1.speed + (1 * incr)
		else:
			ship_1.pos[0] -= WIDTH - 2*ship_1.size
		if ship_2.pos[0] >= ship_2.size and ship_2.pos[0] <= WIDTH - ship_2.size:
			ship_2.pos[0] -= ship_2.speed + (1 * incr)
		else:
			ship_2.pos[0] += WIDTH - 2*ship_2.size
		if ship_3.pos[0] >= ship_3.size and ship_3.pos[0] <= WIDTH - ship_3.size:
			ship_3.pos[0] += ship_3.speed + (1 * incr)
		else:
			ship_3.pos[0] -= WIDTH - 2*ship_3.size
		if ship_4.pos[0] >= ship_4.size and ship_4.pos[0] <= WIDTH - ship_4.size:
			ship_4.pos[0] -= ship_4.speed + (1 * incr)
		else:
			ship_4.pos[0] += WIDTH - 2*ship_4.size
		if ship_5.pos[0] >= ship_5.size and ship_5.pos[0] <= WIDTH - ship_5.size:
			ship_5.pos[0] += ship_5.speed + (1 * incr)
		else:
			ship_5.pos[0] -= WIDTH - 2*ship_5.size
		if ship_6.pos[0] >= ship_6.size and ship_6.pos[0] <= WIDTH - ship_6.size:
			ship_6.pos[0] += ship_6.speed + (1 * incr)
		else:
			ship_6.pos[0] -= WIDTH - 2*ship_6.size


def display_traps():
	screen.blit(trap_1.Image, (trap_1.pos[0], trap_1.pos[1]))       
	screen.blit(trap_2.Image, (trap_2.pos[0], trap_2.pos[1]))
	screen.blit(trap_3.Image, (trap_3.pos[0], trap_3.pos[1]))
	screen.blit(trap_4.Image, (trap_4.pos[0], trap_4.pos[1]))
	screen.blit(trap_5.Image, (trap_5.pos[0], trap_5.pos[1]))


def display_ships():
	screen.blit(ship_1.Image, (ship_1.pos[0], ship_1.pos[1]))  
	screen.blit(ship_2.Image, (ship_2.pos[0], ship_2.pos[1]))
	screen.blit(ship_3.Image, (ship_3.pos[0], ship_3.pos[1]))
	screen.blit(ship_4.Image, (ship_4.pos[0], ship_4.pos[1]))
	screen.blit(ship_5.Image, (ship_5.pos[0], ship_5.pos[1]))
	screen.blit(ship_6.Image, (ship_6.pos[0], ship_6.pos[1]))


def reset_1():
	player_1.pos[0] = WIDTH//2
	player_1.pos[1] = HEIGHT - player_1.Image.get_height()


def reset_2():
	player_2.pos[0] = WIDTH//2
	player_2.pos[1] = 0


def check_collision_1(play, obs):
	a = play.pos[0]
	b = play.pos[1]
	c = obs.pos[0]
	d = obs.pos[1]
	if (c >= a and c < (a + 50)) or (a >= c and a < (c + 50)):
			if (d >= b and d < (b + 50)) or (b >= d and b < (d + 50)):
				return True
			else:
				return False
	else:
		return False


def check_collision_2(play, obs):
	a = play.pos[0]
	b = play.pos[1]
	c = obs.pos[0]
	d = obs.pos[1]
	if (c >= a and c < (a + 50)) or (a >= c and a < (c + 50)):
			if (d >= b and d < (b + 50)) or (b >= d and b < (d + 50)):
				return True
			else:
				return False
	else:
		return False


def check_collision_3(play, obs):
	a = play.pos[0]
	b = play.pos[1]
	c = obs.pos[0]
	d = obs.pos[1]
	if (c >= a and c < (a + 50)) or (a >= c and a < (c + 50)):
			if (d >= b and d < (b + 50)) or (b >= d and b < (d + 50)):
				return True
			else:
				return False
	else:
		return False


def neutralise():
	global pressed_up_1
	global pressed_down_1
	global pressed_right_1
	global pressed_left_1
	global pressed_up_2
	global pressed_down_2
	global pressed_right_2
	global pressed_left_2
	pressed_up_1 = False
	pressed_down_1 = False
	pressed_right_1 = False
	pressed_left_1 = False
	pressed_up_2 = False
	pressed_down_2 = False
	pressed_right_2 = False
	pressed_left_2 = False


def initialise_checkpoints():
	for i in range (0,10):
		checkpoint[i] = True

def play_music():
	pygame.mixer.music.load("music/bgm.mp3")
	pygame.mixer.music.set_volume(0.5)
	pygame.mixer.music.play(-1)


def display_score():
	font = pygame.font.Font(None, 24)
	scoretext = font.render(("Player 1: " + str(no.score1)),True,(0,0,0))
	textRect = scoretext.get_rect()
	textRect.bottomright = [1500, 900]
	screen.blit(scoretext, textRect)
	scoretext = font.render(("Player 2: " + str(no.score2)),True,(0,0,0))
	textRect = scoretext.get_rect()
	textRect.topleft = [0, 0]
	screen.blit(scoretext, textRect)


def g_o():
	screen.fill(floor_color)
	font = pygame.font.Font(None, 80)
	scoretext = font.render("GAME OVER",True,(0,0,0))
	textRect = scoretext.get_rect()
	textRect.center = [750, 450]
	screen.blit(scoretext, textRect)