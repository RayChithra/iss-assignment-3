# ISS Assignment 3

The Game:
    Help Jerry and Nibbles reach their homes alternatively. Watch out for Tom and keep an eye out for the mousetraps.
    The game ends when Jerry and Nibbles are caught consecutively.
Controls:
    Control Nibbles using the arrow keys and Jerry using the wasd keys.
Points:
    Points are assigned based on how far a player gets in a round, the maximum being 425 if you complete the round.
    
    
P.S: Tom tends to speed up as more and more rounds pass.


                  Here's to an overwhelming victory of rodents over felines.